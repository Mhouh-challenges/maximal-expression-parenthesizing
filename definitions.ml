type operator =
  | Addition
  | Substraction
  | Multiplication

type expression =
  | Value of int
  | Operation of operation

and operation = {
  left: expression;
  operator: operator;
  right: expression;
}

let function_of_operator = function
  | Addition -> ( + )
  | Multiplication -> ( * )
  | Substraction -> ( - )

let rec evaluate = function
  | Value v -> v
  | Operation { left; operator; right } ->
    function_of_operator operator
      (evaluate left)
      (evaluate right)

let rec print out = function
  | Value v -> Printf.fprintf out "%d" v
  | Operation { left; operator; right } ->
    let operator_char = 
      match operator with
      | Addition -> '+'
      | Substraction -> '-'
      | Multiplication -> '*' in
    Printf.fprintf out "(%a %c %a)"
      print left
      operator_char
      print right

let rotate_right = function
  | Value _
  | Operation { left = Value _; _ } ->
    None
  | Operation { left = Operation { left = left'; operator = operator'; right = right' }; operator; right } ->
    Some (Operation { left = left'; operator = operator'; right = Operation { left = right'; operator; right }})

let ( let* ) = Option.bind

let rec reorders expression =
  match expression with
  | Value _ -> [ expression ]
  | Operation { left; operator; right; } ->
    List.map
      (fun left ->
         List.map
           (fun right -> Operation { left; operator; right })
           (reorders right))
      (reorders left)
    |> List.concat
    |> List.append ((let* rotated = rotate_right expression in Some (reorders rotated)) |> Option.value ~default:[])

let process expression = 
  let reorders = reorders expression in
  Printf.printf "%d reorderings:\n" (List.length reorders);
  List.iter (fun expression ->
      let value = evaluate expression in
      Printf.printf "%a = %d\n" print expression value;
    ) reorders;
  let difference e1 e2 = evaluate e2 - (evaluate e1) in
  let best = reorders
             |> List.sort difference
             |> List.hd in
  Printf.printf "---\nbest:\n%a = %d\n"
    print best
    (evaluate best)
