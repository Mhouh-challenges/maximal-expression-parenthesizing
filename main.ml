let expression =
  Re.(
    seq [
      group (rep1 any);
      group (alt [ char '-'; char '+'; char '*' ]);
      group (rep1 digit);
    ]
    |> compile
  )

let rec expand expr =
  try
    Re.exec expression expr |> ignore;
    Re.(replace expression ~f expr)
  with Not_found -> "Value " ^ expr

and f groups =
  let get = Re.Group.get groups in
  let operator =
    match String.get (get 2) 0 with
    | '+' -> "Addition"
    | '-' -> "Substraction"
    | '*' -> "Multiplication"
    | _ -> assert false in
  let rest = get 1 in
  let left = expand rest in
  let right = get 3 in
  Printf.sprintf
    "Operation { left = %s; operator = %s; right = Value %s; }"
    left
    operator
    right

let _ =
  print_endline "enter an expression (try 1+2*3-4)";
  let expression = expand (read_line ()) in
  let out = open_out "expression.ml" in
  Printf.fprintf out
    "let () = process (%s)"
    expression;
  close_out out;
  Sys.command "cat definitions.ml expression.ml > generated.ml && ocamlc generated.ml && ocamlrun a.out"
